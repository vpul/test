@extends('layouts.layout')

@section('content')
    <div class="container">
        <form method="post" class="form-resume">
            @csrf
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <div class="form-group row">
                <div class="col-md-3 pr-2 pt-2" style="border-right: 5px solid rgba(54,63,74,0.16); border-bottom: 5px solid rgba(54,63,74,0.16)">
                    <div class="d-flex justify-content-between">
                        <p>Surname</p>
                        <p>{{$user->surname ?? ''}}</p>
                    </div>

                    <div class="d-flex justify-content-between">
                        <p>Name</p>
                        <p>{{$user->name ?? ''}}</p>
                    </div>

                    <div class="d-flex justify-content-between">
                        <p>Last name</p>
                        <p>{{$user->last_name ?? ''}}</p>
                    </div>
                </div>
                <div class="col-md-9 pt-2">
                    <div class="text-center">
                        <h3>Create resume</h3>
                        <strong>You can have only one resume.</strong>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name='title' value="{{$resume->title ?? ''}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Phone</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name='telephone' value="{{$resume->telephone ?? ''}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Skills</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" style="height: 90px">
                                {{$resume->description ?? ''}}
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-1 col-form-label">Education</label>
                <ol style="width: 100%" class="educations">
                    @if(isset($educations) && \count($educations) > 1)
                        @foreach($educations as $education)
                            <li><input type="text" class="form-control education mb-2" name='education' value="{{$education->name}}"></li>
                        @endforeach
                    @else()
                        <li><input type="text" class="form-control education mb-2" name='education' value="{{$educations->first()->name ?? ''}}"></li>
                    @endif
                </ol>
            </div>
            <button type="button" class="add-education">
                Add additional education
            </button>
            <div class="pt-3 text-right">
                <a class="btn btn-primary save" href="#" style="color: white">
                    Save
                </a>
            </div>
        </form>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function () {
            $('.save').click(function (e) {
                e.preventDefault();
                $.ajax({
                    url: "{{route('resume.store')}}",
                    type: 'post',
                    data: $('.form-resume').serialize(),
                    success: function (data) {
                        if (data.success) {
                            window.location.replace("{{route('index')}}");
                        }
                        if (data.failure) {
                            var text = data.errors.join(',');
                            swal({
                                type: 'error',
                                title: 'Oops...',
                                text: text
                            });
                        }
                    }
                });
            });
            $(document).on('change', '.education', function () {
                $.ajax({
                    url: '{!! route('education.store')!!}',
                    data: {
                        'user_id': "{{auth()->user()->id}}",
                        'name': $(this).val()
                    },
                    dataType: 'json',
                    success: function (data) {
                    }
                });
            });
            $(document).on('click', '.add-education', function () {
                $('.educations').append("<li><input type='text' class=\"form-control education mb-2\" name='education' value=''></li>");
            })
        })

    </script>
@endpush
