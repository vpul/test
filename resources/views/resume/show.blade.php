@extends('layouts.layout')

@section('content')
    <div class="container">
        <form method="post" class="form-resume">
            @csrf
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <div class="form-group row">
                <div class="col-md-3 pr-2 pt-2" style="border-right: 5px solid rgba(54,63,74,0.16); border-bottom: 5px solid rgba(54,63,74,0.16)">
                    <div class="d-flex justify-content-between">
                        <p>Surname</p>
                        <p>{{$user->surname ?? ''}}</p>
                    </div>

                    <div class="d-flex justify-content-between">
                        <p>Name</p>
                        <p>{{$user->name ?? ''}}</p>
                    </div>

                    <div class="d-flex justify-content-between">
                        <p>Last name</p>
                        <p>{{$user->last_name ?? ''}}</p>
                    </div>
                </div>
                <div class="col-md-9 pt-2">
                    <div class="text-center pt-2">
                        <h3>{{$resume->title}}</h3>
                    </div>
                </div>
            </div>

            <div>
                <div>
                    <p class="mt-1">Phone: {{$resume->telephone ?? ''}}</p>
                </div>
            </div>
            <div>
                <div>
                    <p class="mt-1">Skils: {{$resume->description ?? ''}}</p>
                </div>
            </div>
            <div>
                <p>Education:</p>
                <ol>
                    @if(count($educations) > 0)
                        @foreach ($educations as $education)
                            <li>{{$education->name ?? ''}}</li>
                        @endforeach
                    @endif
                </ol>
            </div>
            <div class="pt-3">
                <a class="btn btn-primary" href="{{route('index')}}" style="color: white">
                    Back
                </a>
            </div>
        </form>
    </div>
@endsection

