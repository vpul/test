@extends('layouts.layout')

@section('content')

    <main role="main">
        <div class="container">
            <section class="jumbotron text-center" style="padding: 2rem 1rem">
                <div class="container">
                    <div class="row">
                        <?php $guest = auth()->check() ? false : true ?>
                        <div class="{{$guest ? 'col-md-12' : 'col-md-10'}}">
                            <h1 class="jumbotron-heading">Here are the latest created summaries.</h1>
                        </div>
                        @if(!$guest)
                            <div class="col-md-2">
                                <a href="#" data-resume-exist="{{$exist}}" class="btn btn-primary my-2 j-create">Create resume</a>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
        <div class="album  bg-light">
            <div class="container">
                <div class="row">
                    @if(\count($resume) > 0)
                        @foreach($resume as $item)
                            <div class="col-md-4">
                                <div class="card mb-4 shadow-sm">
                                    <div class="card-body">
                                        <h3>{{$item->title ?? ''}}</h3>
                                        <p class="card-text">{{$item->description ?? ''}}</p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="btn-group">
                                                <a type="button" href="{{route('resume.show', ['id' => $item->id])}}" class="btn btn-sm btn-outline-secondary">View</a>
                                                @if(!auth()->guest() && auth()->user()->id == $item->user_id)
                                                    <a type="button" href="{{route('resume.edit', ['id' => $item->id])}}" class="btn btn-sm btn-outline-secondary ml-2">Edit</a>
                                                @endif
                                            </div>
                                            <small class="text-muted">{!! date_format($item->created_at, 'd/m/Y') !!}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </main>

    <footer class="text-muted">
        <div class="container text-center">
            <p>Example is &copy; Bootstrap, but please download and customize it for yourself!</p>
            <p>New to Bootstrap? <a href="https://getbootstrap.com/">Visit the homepage</a> or read our <a href="/docs/4.3/getting-started/introduction/">getting started guide</a>.</p>
        </div>
    </footer>
@endsection
@push('script')
    <script>
        $(document).on('click', '.j-create', function (e) {
            e.preventDefault();
            if ($(this).attr('data-resume-exist') == 1) {
                swal({
                    text: 'You already have a resume',
                    align: 'center'
                });
            } else {
                window.location.href = "{{route('resume.create')}}";
            }

        })
    </script>
@endpush
