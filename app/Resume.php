<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    protected $table = 'resume';

    public $fillable = [
        'title',
        'telephone',
        'user_id',
        'description'
    ];
}
