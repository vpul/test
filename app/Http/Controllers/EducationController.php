<?php

namespace App\Http\Controllers;

use App\Education;
use App\Http\Requests\Request;

class EducationController extends Controller
{
    public function store(Request $request)
    {
        $model = new Education();
        $model->user_id = $request->input('user_id');
        $model->name = $request->input('name');

        if ($model->save()) {
            return response()->json([
                'success' => true
            ]);
        }

        return false;
    }
}