<?php

namespace App\Http\Controllers;

use App\Education;
use App\Http\Requests\Request;
use App\Resume;
use App\User;
use http\Env\Response;
use Illuminate\Support\Facades\Validator;

class ResumeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $resume = Resume::orderBy('created_at', 'DESC')
            ->get();
        if (auth()->check()) {
            $exist = Resume::where('user_id', auth()->user()->id)->first() ? 1 : 0;
        } else {
            $exist = false;
        }

        return view('index', [
            'resume' => $resume,
            'exist' => $exist
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $user = auth()->user();

        return view('resume.create', [
            'user' => $user,
        ]);
    }

    public function edit(Resume $resume)
    {
        $user = auth()->user();
        $educations = Education::where('user_id', $user->id)
            ->get();
        return view('resume.create', [
            'user' => $user,
            'resume' => $resume,
            'educations' => $educations
        ]);
    }

    /**
     * @param Resume $resume
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Resume $resume)
    {
        $educations = Education::where('user_id', $resume->user_id)
            ->get();
        $user = User::select('name', 'surname', 'last_name')
            ->where('id', $resume->user_id)
            ->first();
        return view('resume.show', [
            'resume' => $resume,
            'educations' => $educations,
            'user' => $user
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->only(['telephone', 'title', 'user_id', 'description']);
        $validator = Validator::make($data, [
            'telephone' => 'required',
            'title' => 'required',
            'user_id' => 'unique:resume'
        ]);

        if (\count($errors = $validator->errors()->all()) > 0) {
            return response()->json([
                'failure' => true,
                'errors' => $errors,
            ]);
        }

        if ($request->isMethod('POST')) {
            if ($model = Resume::create($data)) {
                return \response()->json([
                    'success' => true,
                    'resume' => $model
                ]);
            }
        }
    }

    public function update(Request $request)
    {
        $data = $request->only(['telephone', 'title', 'description']);
        $validator = Validator::make($data, [
            'telephone' => 'required',
            'title' => 'required'
        ]);

        if (\count($errors = $validator->errors()->all()) > 0) {
            return response()->json([
                'failure' => true,
                'errors' => $errors,
            ]);
        }

        if ($request->isMethod('PUT')) {
            if ($model = Resume::updated($data)) {
                return \response()->json([
                    'success' => true,
                    'resume' => $model
                ]);
            }
        }
    }
}
