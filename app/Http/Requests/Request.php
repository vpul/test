<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Request extends FormRequest
{
    /**
     * @var \Illuminate\Support\MessageBag
     */
    protected $errors;
    /**
     * @var array
     */
    protected $rules = [];

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function getValidator()
    {
        return $this->getValidatorInstance();
    }

    /**
     * @return bool
     */
    public function fails()
    {
        $result = $this->getValidatorInstance()->fails();
        $this->errors = $this->getValidatorInstance()->errors();

        return $result;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    /**
     * @param $name
     * @param $validator
     */
    public function addRule($name, $validator)
    {
        $this->rules[$name] = $validator;
    }

    /**
     * @param $name
     */
    public function removeRule($name)
    {
        if (isset($this->rules[$name])) {
            unset($this->rules[$name]);
        }
    }

    /**
     * @return \Illuminate\Support\MessageBag
     */
    public function errors(): \Illuminate\Support\MessageBag
    {
        if (!$this->errors) {
            return new \Illuminate\Support\MessageBag;
        }

        return $this->errors;
    }
}