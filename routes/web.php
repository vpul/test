<?php

Route::get('/', [
    'uses' => 'ResumeController@index',
    'as' => 'index'
]);

Route::group(['prefix' => 'resume', 'middleware' => 'auth'], function (){
    Route::get('/create', [
        'uses' => 'ResumeController@create',
        'as' => 'resume.create'
    ]);
    Route::get('/{resume}/edit', [
        'uses' => 'ResumeController@edit',
        'as' => 'resume.edit'
    ]);
    Route::get('/{resume}', [
        'uses' => 'ResumeController@show',
        'as' => 'resume.show'
    ]);
    Route::post('/store', [
        'uses' => 'ResumeController@store',
        'as' => 'resume.store'
    ]);
    Route::put('/update', [
        'uses' => 'ResumeController@update',
        'as' => 'resume.update'
    ]);
});

Route::get('/education.create', [
    'uses' => 'EducationController@store',
    'as' => 'education.store'
]);

Auth::routes();

Route::get('/login', function (){
    return view('auth.login');
})->name('login');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
